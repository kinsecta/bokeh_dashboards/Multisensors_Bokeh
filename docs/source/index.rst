.. KInsect - Bokeh App documentation master file, created by
   sphinx-quickstart on Tue Apr 20 11:31:46 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to KInsecta's Multisensors Bokeh App
============================================
Visit us on `GitLab <https://gitlab.com/kinsecta/bokeh_dashboards/Multisensors_Bokeh>`_.


Information
===========
This documentation gives an overview of the project's structur, the used GUI classes - rendered as tabs in the Bokeh app - 
and the classes respectively functions to pass sensor data to the visualization classes.   


Contents
========

.. toctree::
   :maxdepth: 4
   
   modules/sensors.rst
   modules/sensors.Utilities.rst
   modules/sensors.GUIclasses.rst   
 

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

sensors
=======

.. automodule:: sensors
   :members:
   :special-members: __init__

app\_hooks.py
-------------

.. automodule:: sensors.app_hooks
   :members:
   :undoc-members:
   :show-inheritance:

main.py
-------

.. automodule:: sensors.main
   :members:
   :undoc-members:
   :show-inheritance:

.. _config.yaml:

config.yaml
-----------

Dictionary that contains various parameters for the control of the picamera and the wingbeat sensor.

.. include:: ../../../sensors/config.yaml
   :literal:

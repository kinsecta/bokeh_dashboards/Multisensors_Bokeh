sensors.GUIclasses
==================

.. automodule:: sensors.GUIclasses
   :members:
   :special-members: __init__

Class InfoTab
-------------

.. automodule:: sensors.GUIclasses.info
   :members:
   :undoc-members:
   :show-inheritance:

Class CamStreamTab
------------------

.. automodule:: sensors.GUIclasses.picam_stream
   :members:
   :undoc-members:
   :show-inheritance:

Class AudioStreamTab 
--------------------

.. automodule:: sensors.GUIclasses.audio_stream
   :members:
   :undoc-members:
   :show-inheritance:

Class TriggeredSaveTab
----------------------

.. automodule:: sensors.GUIclasses.triggered_save
   :members:
   :undoc-members:
   :show-inheritance:

Class SingleSave
----------------

.. automodule:: sensors.GUIclasses.single_save
   :members:
   :undoc-members:
   :show-inheritance:

Class DataViewerTab
-------------------

.. automodule:: sensors.GUIclasses.data_viewer
   :members:
   :undoc-members:
   :show-inheritance:

Class AutocompleteTaxonomy
--------------------------

.. automodule:: sensors.GUIclasses.taxonomy_autocomplete_widget
   :members:
   :undoc-members:
   :show-inheritance:

Class CameraSettings
--------------------

.. automodule:: sensors.GUIclasses.camera_settings_widget
   :members:
   :undoc-members:
   :show-inheritance:

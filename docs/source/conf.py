# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('../..'))
#print(os.path.abspath('../../app'))


# -- Project information -----------------------------------------------------

project = 'KInsecta\'s Multisensors Bokeh App'
copyright = '2021, M.Tschaikner'
author = 'M.Tschaikner'
html_logo = 'kinsecta_logo.svg'

# The full version, including alpha/beta/rc tags
release = '0.0.1'


# -- General configuration ---------------------------------------------------
autodoc_mock_imports = [
    "pyaudio", "RPi.GPIO", "soundcard", "picamerax", "smbus2", "cv2", "bme280", "adafruit_pm25", "adafruit_bh1750",
    "adafruit_as7341",
] 
#    "sensors.GUIclasses.taxonomy_autocomplete_widget",
#    "sensors.GUIclasses.camera_settings_widget",
#]


# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.intersphinx",
    "sphinx.ext.autodoc",
    "sphinx.ext.mathjax",
    "sphinx.ext.viewcode",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = []

pigments_style = 'sphinx'

#autodoc_default_options = {
#    'members': True,
#    'member-order': 'bysource',
#    'special-members': True,
#    'undoc-members': True,
#    'exclude-members': '__weakref__',
#}

language = 'en'

Installation of KInsect's Multisensors Bokeh app 
================================================
 
1) Install latest Raspbian lite (python3.7.3 and pulseaudio already installed April 26th 2021) 
2) `git clone https://gitlab.com/kinsect/bokeh_dashboards/Multisensors_Bokeh` to install repository in current directory
2) `cd Multisensors_Bokeh` to switch into Multisensors_Bokeh folder
4) `pip3 install -r bokeh_requirements.txt` to download and install required packages 
5) *optional:* `nano bokeh_requirements.txt` to view required packages
6) restart system

7) If a numpy error `libf77blas.so.3` raises when trying to start Multisensors Bokeh app:
```
sudo apt-get install libatlas-base-dev
```
to install missing library.

8) If `libraries: libportaudio.so.2` can't be loaded:
```
sudo apt-get install python-pyaudio
sudo apt-get install libasound-dev
```
to install missing libraries.

9) Install additional packages if other errors raise: 
```
sudo apt-get install -y build-essential tk-dev libncurses5-dev libncursesw5-dev libreadline6-dev libdb5.3-dev libgdbm-dev libsqlite3-dev libssl-dev libbz2-dev libexpat1-dev liblzma-dev zlib1g-dev libffi-dev libatlas-base-dev
sudo apt-get install -y pulseaudio
```
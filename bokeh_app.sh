#!/usr/bin/bash
# example of a shell script to open bokeh app, change ip:port appropriately to YOUR settings

# start Jack instance to avoid pyaudio warnings
# jack_control start

# get bokeh.sh path
DIR="$( cd "$( dirname "$0" )" && pwd)"

# first two lines activate virtual environment, comment out or change first line appropriately to YOUR paths
cd /home/pi/venv/bokeh/bin
source activate

# start bokeh server
cd ${DIR}
# sleep 10 &
bokeh serve --show sensors --check-unused-sessions 1000 --unused-session-lifetime 1000 

# open browser window if served at given websocket-origin
# bokeh serve sensors --allow-websocket-origin=192.168.137.2:5006 --check-unused-sessions 1000 --unused-session-lifetime 1000 &
# DISPLAY=:0 chromium-browser '192.168.137.2:5006'

# exit Jack instance
# jack_control exit

# sleep 2

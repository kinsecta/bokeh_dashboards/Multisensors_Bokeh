"""
Contains functions that get called on server start and on creation respectively destruction of a client side session.
"""

import os
import sys
import RPi.GPIO as GPIO
import yaml
from attrd import AttrDict
# used to set absolut path for definition of sensors module
sys.path.insert(0, os.path.abspath('.'))
from sensors.Utilities.initdevices import InitDevices


def on_server_loaded(server_context):
    """Function gets called when a Bokeh server is instantiated. It loads the config.yaml information
    as dictionary and add it to Bokeh's server_context attribute. Also initializes the devices and a
    corresponding atribute is also added to server_context to pass it to the Bokeh server instance.
    """
    
    if os.path.exists('sensors/config.yaml'):
        with open('sensors/config.yaml') as f:
            config = AttrDict(yaml.safe_load(f))
            f.close()
            print('Loaded configuration file!')
    else:
        print('No configuration file found!')
        exit(0)
    server_context.config = config
    server_context.all_devices = InitDevices(config)


def on_server_unloaded(server_context):
    """Function gets called when a Bokeh server cleanly exits. Sets GPIO switch for LED to OFF.
    """
    
    GPIO.output(server_context.config.GPIO_Switch, GPIO.LOW)


def on_session_created(session_context):
    """Function gets called when a new session is created. It gets the number of the currently opened sessions. The
       first opened session is flagged as master session and gains controls over the sensor devices. A flag for the
       master session and a counter for added ViewerTabs is added to session_context to pass it to the session instance.
    """
    
    num_sessions = len(session_context.server_context.sessions)
    if num_sessions == 0:
        print('First active session opened! Sensor control available!')
    else:
        print(num_sessions, 'other active session(s) opened! No sensor control available!')
    # True for first session, passed to main.py
    session_context.request.master_session = (len(session_context.server_context.sessions) == 0)
    # Reset counter on ViewerTab when new session is opened, passed to main.py
    session_context.request.viewer_counter = 0
    pass


def on_session_destroyed(session_context):
    """Function gets called when a session is closed. If the master session gets closed the PyAudiohandler thread
       will be killed to free up resources.
    """
    
    num_sessions = len(session_context.server_context.sessions)
    try:
        session_context.audio.kill()
        print('Master Session closed! Old PyAudiohandler thread killed!')
    except:
        if num_sessions == 0:
            print('Master Session closed!')
        else:
            print('Viewer Session closed!')
    print(num_sessions, 'active session(s) opened!')
    pass

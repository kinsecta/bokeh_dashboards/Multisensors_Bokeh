import numpy as np


class RingBuffer:
    """Creates a 1D ring buffer object using numpy arrays.

    :param length: length of ring buffer
    :type length: int
    """
    
    def __init__(self, length):
        self.data = np.zeros(length, dtype='f')
        self.index = 0

    def extend(self, array):
        """Writes the array into the ring buffer.

        :param array: added array 
        :type array: 1D float array
        """
        
        x_index = (self.index + np.arange(array.size)) % self.data.size
        self.data[x_index] = array
        self.index = x_index[-1] + 1

    def get(self):
        """Returns the ring buffer.
        
        :return: ring buffer
        :rtype: 1D float array
        """
        
        idx = (self.index + np.arange(self.data.size)) % self.data.size
        return self.data[idx]

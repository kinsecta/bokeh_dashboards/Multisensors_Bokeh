import bme280


class WriteMetaData:
    """Class to write meta data to `data.json`.

    :param config: configuration including sensor objects
    :type config: object
    :param data_json: json dictionary with import/output-format for KInsecta web app
    :type data_json: dictionary
    """

    def __init__(self, config, data_json):

        self.bme280_sensor = config.bme280_sensor
        self.bh1750_sensor = config.bh1750_sensor
        self.as7341_sensor = config.as7341_sensor
        self.pmsa003i_sensor = config.pmsa003i_sensor
        self.data_json = data_json

    def write_humidity_temperature_air_pressure(self):

        if self.bme280_sensor[2] is not None:
            data = bme280.sample(self.bme280_sensor[0], self.bme280_sensor[1], self.bme280_sensor[2])
            self.data_json["temperature"]["value"] = float("{:2.1f}".format(round(data.temperature, 1)))
            self.data_json["air_pressure"]["value"] = float("{:1.3f}".format(round(data.pressure, 0)/1000))
            self.data_json["humidity"]["value"] = float("{:3.1f}".format(round(data.humidity, 1)))

    def write_luminosity(self):

        if self.bh1750_sensor is not None:
            luminosity = float("{:4.1f}".format(self.bh1750_sensor.lux))
            self.data_json["luminosity"]["value"] = luminosity

    def write_spectrum(self):

        if self.as7341_sensor is not None:
            self.data_json["spectrum"]["channel_415nm"] = self.as7341_sensor.channel_415nm
            self.data_json["spectrum"]["channel_445nm"] = self.as7341_sensor.channel_445nm
            self.data_json["spectrum"]["channel_480nm"] = self.as7341_sensor.channel_480nm
            self.data_json["spectrum"]["channel_515nm"] = self.as7341_sensor.channel_515nm
            self.data_json["spectrum"]["channel_555nm"] = self.as7341_sensor.channel_555nm
            self.data_json["spectrum"]["channel_590nm"] = self.as7341_sensor.channel_590nm
            self.data_json["spectrum"]["channel_630nm"] = self.as7341_sensor.channel_630nm
            self.data_json["spectrum"]["channel_680nm"] = self.as7341_sensor.channel_680nm
            self.data_json["spectrum"]["clear_light"] = self.as7341_sensor.channel_clear
            self.data_json["spectrum"]["near_IR"] = self.as7341_sensor.channel_nir

    def write_particulate_matter(self):

        if self.pmsa003i_sensor is not None:
            aq_data = self.pmsa003i_sensor.read()
            self.data_json["particulate_matter"]["pm1"] = aq_data["pm10 standard"]
            self.data_json["particulate_matter"]["pm2p5"] = aq_data["pm25 standard"]
            self.data_json["particulate_matter"]["pm10"] = aq_data["pm100 standard"]
            self.data_json["particulate_matter"]["particles_03um"] = aq_data["particles 03um"]
            self.data_json["particulate_matter"]["particles_05um"] = aq_data["particles 05um"]
            self.data_json["particulate_matter"]["particles_10um"] = aq_data["particles 10um"]
            self.data_json["particulate_matter"]["particles_25um"] = aq_data["particles 25um"]
            self.data_json["particulate_matter"]["particles_50um"] = aq_data["particles 50um"]
            self.data_json["particulate_matter"]["particles_100um"] = aq_data["particles 100um"]

    def write_size_insect(self):
        pass

    def write_rainfall(self):
        pass

    def write_wind_sensor(self):
        pass

    def write_sensor_location(self):
        pass

    def write_all_data(self):
        # implemented
        self.write_humidity_temperature_air_pressure()
        self.write_luminosity()
        self.write_spectrum()
        self.write_particulate_matter()
        # not implemented
        # self.write_size_insect()
        # self.write_rainfall()
        # self.write_wind_sensor()
        # self.write_sensor_location()

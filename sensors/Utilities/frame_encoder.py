import cv2
import numpy as np
import os
import io
import uuid
from time import time
import copy


class FrameEncoder:
    """Class to encode frames from h264 video.

    :param config: configuration including data.json template
    :type config: object
    :param video_path: folder containing video file
    :type video_path: string
    :param data_json: json dictionary with import/output-format for KInsecta web app
    :type data_json: dict
    """
    def __init__(self, config, video_path, data_json):

        self.config = config
        self.video_path = video_path
        self.video_filename = self.get_video_filename()
        self.data_json = data_json
        self.trigger_save = False

    def get_video_filename(self):
        f = []
        for (dir_path, dir_names, filenames) in os.walk(self.video_path):
            f.extend(filenames)
            break
        return os.path.join(self.video_path, f[0])

    def write_frame(self, frame):
        UUID = uuid.uuid1()
        UUID_filename = os.path.join(self.video_path, '{}.{}'.format(UUID, 'png'))
        images_copy = copy.deepcopy(self.config.data["images"])[0]
        images_copy["filename"] = '{}.{}'.format(UUID, 'png')
        self.data_json["images"].append(images_copy)
        print(UUID_filename)
        cv2.imwrite(UUID_filename, frame)

    def frames(self):

        # empty images list and add list entries for multiple images by appending
        self.data_json["images"] = []
        vidcap = cv2.VideoCapture(self.video_filename)
        count = 1
        saved_frames_count, mean_old, rms= 0, 0, 0
        start_time = time()
        frames = []

        while True:
            grabbed, frame = vidcap.read()
            frames.append(frame)

            try:
                # iterate through frames to compute simple trigger statistic,
                # start saving of frames sequence in else case
                if not self.trigger_save:
                    mean_new = np.mean(frame)
                    # skip first loop due to non existing difference
                    if count != 1:
                        mean_diff = mean_new - mean_old
                        # set reasonable starting value for rms
                        if count == 2:
                            rms = np.abs(mean_diff)
                            if rms > 5:
                                rms = 5
                            if rms < 0.05:
                                rms = 0.5
                        # trigger save frames sequence
                        if mean_diff > 5 * rms:
                            self.trigger_save = True
                            # print("{:1.4f}".format(mean_diff), "{:1.4f}".format(rms))
                            print('Sequence saved to:')
                        # ignore great negative differences and don't update rms
                        elif mean_diff < - 5 * rms:
                            pass
                        # update rms
                        else:
                            rms = np.sqrt((np.square(rms) * (count - 1) + np.square(mean_diff)) / count)
                            # print("{:1.4f}".format(mean_diff), "{:1.4f}".format(rms))

                    mean_old = mean_new
                else:
                    if saved_frames_count < self.config.number_frames:
                        self.write_frame(frame)
                        saved_frames_count += 1
                    else:
                        break
            except:
                pass

            if not grabbed:
                break

            count += 1

        # check if save sequence got triggered while iterating through frames. If not then frames from
        # the middle of the h264 video get saved
        if not self.trigger_save:
            index_start = int(count/2)
            indices = list(range(index_start, index_start + self.config.number_frames))
            for index in indices:
                self.write_frame(frames[index])
            encoding_time = "{:2.1f}".format(time() - start_time)
            print('From {} total video frames encoded frames #{}-#{} in {} seconds!'.
                  format(count, index_start, index_start + self.config.number_frames - 1, encoding_time))
        else:
            encoding_time = "{:2.1f}".format(time() - start_time)
            print('Frames #{}-#{} were encoded in {} seconds!'.
                  format(count - self.config.number_frames + 1, count, encoding_time))

        vidcap.release()

        return self.data_json

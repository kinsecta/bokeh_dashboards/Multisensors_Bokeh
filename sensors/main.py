"""
This file gets executed when a new client side session is created. The functions of :file:`app_hook.py` 
will already have returned the corresponding objects and variables that are needed to run this script.
"""

# os methods for manipulating paths
import os
import sys
# json, OrderedDict to get data format for Import/Export database
import json
from collections import OrderedDict
# Bokeh basics 
from bokeh.io import curdoc
from bokeh.models.widgets import Tabs
from bokeh.models import Button, Div
from bokeh.layouts import column, row

# import class for audio data handling
from sensors.Utilities.pyaudiohandler import PyAudioHandler

# import tab classes
from sensors.GUIclasses.info import InfoTab
from sensors.GUIclasses.picam_stream import CamStreamTab
from sensors.GUIclasses.audio_stream import AudioStreamTab
from sensors.GUIclasses.triggered_save import TriggeredSaveTab
from sensors.GUIclasses.single_save import SingleSaveTab
from sensors.GUIclasses.data_viewer import DataViewerTab

import RPi.GPIO as GPIO
import threading


def panelActive(attr, old, new):
    """Function gets called on tab change. It deactivates the ringbuffer update if its tab
       is not active to save resources. Analogous behaviour for triggered signal tab.
    """

    # only execute if audio device found
    if all_devices.audio_devices[1] is not None:
        if all_tabs.active == 0:
            if visual_ringbuffer.btn_wingbeats.label == 'Stop Stream':
                visual_ringbuffer.do_update()
        elif all_tabs.active == 1:
            if visual_ringbuffer.btn_wingbeats.label == 'Stop Stream':
                visual_ringbuffer.do_update()
        elif all_tabs.active == 2:
            if visual_ringbuffer.btn_wingbeats.label == 'Stop Stream':
                visual_ringbuffer.do_update()
        elif all_tabs.active == 3:
            # if visual_camera.btn_preview.label == 'Stop Stream':
            # visual_camera.do_preview()
            if visual_trigger.btn_wingbeats.label == 'Stop Stream':
                visual_trigger.do_update()
        else:
            if visual_ringbuffer.btn_wingbeats.label == 'Stop Stream':
                visual_ringbuffer.do_update()
            if visual_trigger.btn_wingbeats.label == 'Stop Stream':
                visual_trigger.do_update()


def add_viewer_tab():
    """Function gets called on button click. It adds an extra ViewerTab and updates
       the Bokeh visualization.
    """

    curdoc().session_context.request.viewer_counter += 1
    all_tabs.tabs.append(DataViewerTab(config).tab)
    all_tabs.update(tabs=all_tabs.tabs)


def shutdown_server():
    """Function gets called on button click.
    """

    print('Shutting down Bokeh server!')
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(config.GPIO_Switch, GPIO.OUT)
    GPIO.output(config.GPIO_Switch, GPIO.LOW)
    
    exit(0)


# the following lines of code get executed if a bokeh server is instantiated,
# but will be ignored if Sphinx is creating an auto documentation, "docs" for local and "source" for RTD html creation
if os.path.basename(os.path.normpath(os.path.abspath('.'))) == 'docs' or \
        os.path.basename(os.path.normpath(os.path.abspath('.'))) == 'source':
    print('Sphinx automatic api documentation running!')
else:
    # get config from on_server_load in app hooks
    config = curdoc().session_context.server_context.config
    # add curdoc to config to pass to tab objects
    config.curdoc = curdoc()
    # initialize data structure JSON file and add to config and pass to tab objects
    if os.path.exists('sensors/templates/data_template.json'):
        with open('sensors/templates/data_template.json', encoding='UTF-8') as f:
            data_template_dict = json.load(f, object_pairs_hook=OrderedDict)
            data_template_dict["sensor_id"] = config.sensor_id
            f.close()
            print('Loaded data format!')
    else:
        print('No data format template file found!')
        exit(0)
    config.data = data_template_dict
    # initialize species JSON file and add to config and pass to tab objects
    if os.path.exists('sensors/templates/species_template.json'):
        with open('sensors/templates/species_template.json', encoding='UTF-8') as f:
            species_template_dict = json.load(f, object_pairs_hook=OrderedDict)
            f.close()
            print('Loaded species list!')
    else:
        print('No data format template file found!')
        exit(0)
    config.species = species_template_dict
    
    # get device object from server context (app_hooks.py)
    all_devices = curdoc().session_context.server_context.all_devices
    
    # initialize tab list
    tabs_list = list()
    # add info tab
    tabs_list.append(InfoTab(all_devices).tab)
    # include control tabs if it's the only active session (app_hooks.py) and corresponding devices are connected 
    # reload is not working atm, because always 1 active session existent (destruction of old session happening
    # after creation of new one)
    if curdoc().session_context.request.master_session:
        if all_devices.camera is not None:
            visual_camera = CamStreamTab(config, all_devices.camera)
            tabs_list.append(visual_camera.tab)
        if all_devices.audio_devices[1] is not None:
            # visual_trigger = TriggeredSaveTab(config)
            visual_trigger = SingleSaveTab(config)
            tabs_list.append(visual_trigger.tab)
            visual_ringbuffer = AudioStreamTab(config)
            tabs_list.append(visual_ringbuffer.tab)
            # initialize audio thread, save audio object to session_context to kill thread after session destroyed
            audio = PyAudioHandler(visual_trigger, visual_ringbuffer)
            audio.start()
            curdoc().session_context.audio = audio

    # add viewer tab
    tabs_list.append(DataViewerTab(config).tab)
    # create tab object from tab list, only check for active tabs in master session
    all_tabs = Tabs(tabs=tabs_list)
    if curdoc().session_context.request.master_session: 
        all_tabs.on_change('active', panelActive)
    # create exit button
    btn_exit = Button(label='Shutdown server', button_type="danger", width=115)
    btn_exit.on_click(shutdown_server)
    # create extra ViewerTab at button click
    btn_add_tab = Button(label='Add Data Viewer', button_type='primary', width=115)
    btn_add_tab.on_click(add_viewer_tab)
    # add osi logo and final layout of all elements in current document
    logo_osi = """<div><img src="sensors/static/logos/osi.svg" width="32"></div>"""
    logo_kinsecta = """<div><img src="sensors/static/logos/kinsecta.svg" width="32" ></div>"""
    curdoc().add_root(column(row(Div(text=logo_kinsecta), Div(text=logo_osi), btn_exit, btn_add_tab), all_tabs))
    # browser tab name
    curdoc().title = 'KInsecta Multisensors - Bokeh'

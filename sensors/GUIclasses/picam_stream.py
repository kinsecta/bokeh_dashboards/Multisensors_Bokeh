from bokeh.plotting import figure
from bokeh.models import Panel, Button, Div, ColumnDataSource
from bokeh.events import ButtonClick
from bokeh.layouts import layout, column, row, gridplot
from bokeh.document import without_document_lock

from sensors.GUIclasses.taxonomy_autocomplete_widget import AutocompleteTaxonomy
from sensors.GUIclasses.camera_settings_widget import CameraSettings
from sensors.Utilities.frame_encoder import FrameEncoder
from sensors.Utilities.write_meta_data import WriteMetaData

import picamerax as picamera
import io

from tornado import gen
from concurrent.futures import ThreadPoolExecutor
import numpy as np

from time import sleep, time
from datetime import datetime
import os
import RPi.GPIO as GPIO

from colorama import Fore, Back, Style

import uuid
import json
import copy


class CamStreamTab:
    """Class to create a Bokeh tab that displays a life stream from a picamera. Pictures can be taken
    manually or are triggered with a light barrier.
       
    :param config: configuration including :ref:`config.yaml` dictionary and current Bokeh document
    :type config: object
    :param camera: picamera object initialized in :mod:`sensors.app_hooks` with
    :class:`sensors.Utilities.initdevices.InitDevices`
    :type camera: object
    """
    
    def __init__(self, config, camera):
        
        self.config = config
        self.curdoc = config.curdoc
        self.autocomplete_tax = AutocompleteTaxonomy(config)
        self.camera_settings = CameraSettings(config)
        
        self.CAMERA_WIDTH, self.CAMERA_HEIGHT = eval(config.camera_res_preview)
        self.stream_callback = None
        self.update_in_progress = False
        self.save_in_progress = False
        self.camera = camera
        
        self.template = """<div class='content' style="background-color: {background};color: {colour};">
                            <div class='info'> {status_text} </div> </div>"""
        self.template_png = """<div class='image_preview'>"""
        # call tab method to create tab 
        self._tab = self.make_tab()
        self.image_div_list = (self.image_div_1, self.image_div_2, self.image_div_3, self.image_div_4)
        self.plot_counter = 0
        self.executor = ThreadPoolExecutor(max_workers=2)
        
        self.GPIO_Switch = self.config.GPIO_Switch  
        if len(self.config.connected_GPIO_IR_pins) == 0:
            print('No light barriers connected!')
            self.btn_barrier.visible = False
        
        self.barrier = False
        self.trigger = False
        self.trigger_timeout = 5
        self.trigger_time = 0
        self.trigger_start = 0

        self.encode_finished = False
                
    @property
    def tab(self):
        """Calls method :meth:`make_tab`.
        """
        
        return self._tab

    def make_tab(self):
        """ Creates and arranges the elements of the corresponding Bokeh tab.
        
        :return: Bokeh panel for picamera stream visualization 
        :rtype: Bokeh object
        """
        
        # define buttons
        self.btn_preview = Button(label='Start Stream', width=310)
        self.btn_preview.on_event(ButtonClick, self.do_preview)
        self.btn_save = Button(label='Save Image Sequence (%d Images)' % self.config.number_frames, width=310)
        self.btn_save.disabled = True
        self.btn_save.on_event(ButtonClick, self.manual_image_save)
        self.btn_barrier = Button(label='Activate Light Barrier', width=200)
        self.btn_barrier.disabled = True
        self.btn_barrier.on_event(ButtonClick, self.barrier_on_off)
        self.btn_LED = Button(label='Activate LED flash', width=200)
        self.btn_LED.disabled = True
        self.btn_LED.on_event(ButtonClick, self.LED_on_off)
        
        self.text_status = Div(text=self.template.format(status_text='Stream inactive', background='#DC524C',
                                                         colour='#FFFFFF'), render_as_text=False)
        
        # define figure for image data for stream preview 
        self.image = np.empty((self.CAMERA_WIDTH, self.CAMERA_HEIGHT, 4), dtype=np.uint8)
        self.image_source = ColumnDataSource(dict(image=[]))
        self.imagefig = figure(plot_width=self.CAMERA_WIDTH, plot_height=self.CAMERA_HEIGHT,
                               x_range=(0, self.CAMERA_WIDTH), y_range=(0, self.CAMERA_HEIGHT),
                               x_axis_type=None, y_axis_type=None, tools="", toolbar_location=None, name='image')
        self.imagefig.background_fill_color = "#eaeaea"
        self.imagefig.outline_line_color = '#595959'
        # fill imagefig with rgba image data source
        self.imagefig.image_rgba('image', x=0, y=0, dw=self.CAMERA_WIDTH, dh=self.CAMERA_HEIGHT,
                                 source=self.image_source)
        
        # define div containers for preview of last 4 images taken/triggered
        self.image_div_1 = Div(text=self.template_png)
        self.image_div_2 = Div(text=self.template_png)
        self.image_div_3 = Div(text=self.template_png)
        self.image_div_4 = Div(text=self.template_png)
        
        # align buttons, image preview and taxonomy widget
        start_preview = column(self.btn_preview, self.imagefig, self.autocomplete_tax.widget)

        # grid of last 4 saved images
        grid = gridplot([self.image_div_1, self.image_div_2, self.image_div_3, self.image_div_4],
                        ncols=2, merge_tools=False)
        save_grid = column(self.btn_save, grid)
        status_LED_barrier = column(self.text_status, self.btn_LED, self.btn_barrier)
        # put layout together
        _layout = layout(
                        column(row(start_preview, status_LED_barrier, save_grid), self.camera_settings.widget)
        )
        
        return Panel(child=_layout, title='PiCamera')

    def check_light_barrier_interruption(self):
        """ The method checks if any GPIO pin that is connected to a light barrier is HIGH which
        corresponds to an interruption.
        
        :return: Status flag of light barrier interruption
        :rtype: boolean
        """
        
        for pin in self.config.connected_GPIO_IR_pins:
            if GPIO.input(pin) == GPIO.HIGH:
                return True
        
        return False
    
    def init_circular_stream(self):
        """ Initializes a circular buffer for recording a h264 video. Parameters can be tuned via Picamera widget.
        """
        
        self.camera.framerate = self.config.fps_preview
        self.camera.resolution = eval(self.config.video_resolution)
        self.camera.shutter_speed = self.camera_settings.shutter_speed.value
        self.camera.exposure_mode = 'off'
        self.camera.analog_gain = self.camera_settings.analog_gain.value
        self.camera.digital_gain = self.camera_settings.digital_gain.value
        self.camera.awb_mode = 'off'
        self.camera.awb_gains = (self.camera_settings.white_balance_1.value, self.camera_settings.white_balance_2.value)
        self.circular_stream = picamera.PiCameraCircularIO(self.camera, seconds=self.config.video_length,
                                                           bitrate=25000000)
        self.camera.start_recording(self.circular_stream, format='h264', intra_period=1,  quality=20)
        
#         print('{} {}'.format('d_gain:', self.camera.digital_gain))
#         print('{} {}'.format('a_gain:', self.camera.analog_gain))
#         print('{} {}'.format('s_speed:', self.camera.shutter_speed))
#         print('{} {}'.format('awb_gains:', self.camera.awb_gains))
    
    @gen.coroutine
    @without_document_lock
    def camera_future(self):
        """Targeted by next tick callback from :meth:`periodic_callback`. The method creates a future picamera image
        capture in a parallel thread that is not blocking the main Bokeh visualization thread.
        
        :return: future
        :rtype: object
        """
                   
        # get future object of picamera capture from video port
        self.update_in_progress = yield self.executor.submit(self.camera_capture)
        if self.stream_callback is not None:
            self.curdoc.add_next_tick_callback(self.update_Bokeh)

    def camera_capture(self):
        """ Targeted by ThreadPoolExecutor from :meth:`camera_future`. Method to capture a single image with picamera
        video port.
        
        :return: False flag for image capture in progress (capture done)
        :rtype: bool
        """

        self.camera.capture(self.image, 'rgba', use_video_port=True, resize=eval(self.config.camera_res_preview))
        
        return False

    @gen.coroutine
    def update_Bokeh(self):
        """ Targeted by next tick callback from :meth:`camera_future`. Updates the picamera preview.
        """
             
        view = self.image.view(dtype=np.uint32).reshape(self.CAMERA_HEIGHT, self.CAMERA_WIDTH)
        view = np.flip(view, axis=0)
        self.image_source.data['image'] = [view]

    def periodic_callback(self):
        """Targeted by a Bokeh periodic callback from :meth:`do_preview`. Checks if the light barrier got interrupted
        and sets the corresponding flag. If the current Bokeh document update is done a new camera capture for the
        preview with :meth:`camera_future` gets initiated. If one of the save sequence flags is raised the
        :meth:`save_h264_video` gets called before a new preview image is taken.
        """
    
        # check if barrier is activated and triggered
        if self.barrier:
            if self.check_light_barrier_interruption():
                if self.trigger_time >= self.trigger_timeout:
                    print('Light barrier triggered!')
                    # disable manual save button during video capture and frames encoding
                    self.btn_save.disabled = True
                    self.trigger = True
                    self.trigger_time = 0
                    self.trigger_start = time()
                elif self.trigger_time < self.trigger_timeout/2:
                    self.trigger_time = time() - self.trigger_start
                else:
                    self.trigger_time = time() - self.trigger_start
                    self.trigger = False

        if self.encode_finished:
            # check if encode is finished and plot last encoded frame from h264 video
            # create path to image source starting in root, parent directory of sensors
            source = os.path.join(self.datetime_path, sorted(os.listdir(self.datetime_path))[-2])
            source = source.split(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))[-1]
            div_text_img = '<div class="image_preview"><img src="' + str(source) + '" width="100%" height="100%"></div>'
            self.image_div_list[self.plot_counter].text = div_text_img
            # set counter to cycle through preview plots
            self.plot_counter += 1
            if self.plot_counter == 4:
                self.plot_counter = 0
            self.encode_finished = False
            # enable manual save button again
            self.btn_save.disabled = False

        # let periodic callbacks from bokeh pass if blocking computations are performed to
        # not let callback ticks pile up
        if self.update_in_progress:
            # Bokeh updating in progress, do nothing so js callbacks aren't blocked
            pass       
        # save image sequence OR create camera future that is used to update the bokeh visuals
        else:
            # check if image sequence should be captured before new camera future gets initiated
            # initiating a new image capture while the old capture in progress is not possible with picamera
            # both flags are set to False after capture
            if self.save_in_progress or self.trigger:
                # set flags to false after video capture
                self.trigger, self.save_in_progress = False, False
                self.curdoc.add_next_tick_callback(self.save_h264_video)
            else:
                self.update_in_progress = True
                self.curdoc.add_next_tick_callback(self.camera_future)

    def do_preview(self):
        """ Gets called on button click. Starts respectively ends the preview and changes the text status corresponding
        to button label. 
        """
        
        if self.btn_preview.label == 'Start Stream':
            self.btn_preview.label = 'Stop Stream'
            self.text_status.text = self.template.format(status_text='Stream active', background='#3EA639',
                                                         colour='#FFFFFF')
            # initialize circular buffer
            self.init_circular_stream()
            # callback preview set to 10 ms to check light barrier 
            self.stream_callback = self.curdoc.add_periodic_callback(self.periodic_callback, 10)
            # activate save button
            self.btn_save.disabled = False
            self.btn_barrier.disabled = False
            self.btn_LED.disabled = False

        else:
            self.btn_preview.label = 'Start Stream'
            self.text_status.text = self.template.format(status_text='Stream inactive', background='#DC524C',
                                                         colour='#FFFFFF')
            self.camera.stop_recording()
            self.btn_save.disabled = True
            if self.btn_barrier.label == 'Deactivate Light Barrier':
                self.barrier_on_off()
            self.btn_barrier.disabled = True
            if self.btn_LED.label == 'Deactivate LED flash':
                self.LED_on_off()
            self.btn_LED.disabled = True
            self.barrier = False
            self.btn_barrier.label = 'Activate Light Barrier'
            self.curdoc.remove_periodic_callback(self.stream_callback)
            self.stream_callback = None
    
    def manual_image_save(self):
        """ Gets called on button click. Raises flag to capture an image sequence in :meth:`periodic_callback` to
        call :meth:`save_h264_video`.
        """
        
        self.save_in_progress = True
        # disable manual save button during video capture and frames encoding
        self.btn_save.disabled = True
    
    def write_video(self):
        """ Gets called by :meth:`save_h264_video`. Writes circular stream as h264 video with UUID file name to
        disc.
        """
        
        with self.circular_stream.lock:
            # Find the first header frame in the video
            for frame in self.circular_stream.frames:
                if frame.frame_type == picamera.PiVideoFrameType.sps_header:
                    self.circular_stream.seek(frame.position)
                    break
            # Write the rest of the stream to disk
            UUID_vid = uuid.uuid1()
            self.data_dict_temp["video"]["filename"] = '{}.{}'.format(UUID_vid, 'h264')
            video_filename = os.path.join(self.datetime_path, '{}.{}'.format(UUID_vid, 'h264'))
            with io.open(video_filename, 'wb') as output:
                output.write(self.circular_stream.read())
                output.close()
            print(video_filename)

    def encode_frames_write_data(self):
        """
        """
        # encode frames from h264 video
        h264_encoder = FrameEncoder(self.config, self.datetime_path, self.data_dict_temp)
        self.data_dict_temp = h264_encoder.frames()
        # measure meta data and write into json
        Metadata = WriteMetaData(self.config, self.data_dict_temp)
        Metadata.write_all_data()
        self.data_dict_temp = Metadata.data_json
        # save json in datetime path
        with open(os.path.join(self.datetime_path, 'data.json'), 'w') as outfile:
            json.dump(self.data_dict_temp, outfile, indent=4, ensure_ascii=False)

        return True

    @gen.coroutine
    @without_document_lock
    def save_h264_video(self):
        """Gets called on button click or when triggered by an interruption of the light barrier. Saves a h264 video
         and encodes `n` images at the relative path `p`, where `n` is defined by `number_frames` and `p` by
        `img_path` in :ref:`config.yaml`.
        """

        # prepare data json with taxonomy information and construct directory
        now = datetime.now()
        self.data_dict_temp = self.autocomplete_tax.write_tax_to_data()
        self.data_dict_temp["date_time"] = now.strftime("%Y-%m-%dT%H:%M:%SZ")
        timestamp = now.strftime("%Y-%m-%d_%H-%M-%S")
        folder_name = '{}_{}'.format(self.config.sensor_id, timestamp)
        self.datetime_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), self.config.img_path, folder_name)
        os.makedirs(self.datetime_path)
        GPIO.output(self.GPIO_Switch, GPIO.HIGH)
        self.camera.wait_recording(0.7 * self.config.video_length)
        self.write_video()
        # deactivate LED again if not activated before video capture
        if self.btn_LED.label == 'Activate LED flash':
            GPIO.output(self.GPIO_Switch, GPIO.LOW)
        # encodes number_frames frames from captured h264 video in parallel thread
        self.encode_finished = yield self.executor.submit(self.encode_frames_write_data)

    def barrier_on_off(self):
        """Gets called on button click. Activates respectively deactivates light barrier corresponding to button label.
        The barrier status gets checked in :meth:`periodic_callback`. 
        """
        
        if self.btn_barrier.label == 'Activate Light Barrier':
            self.btn_barrier.label = 'Deactivate Light Barrier'
            self.barrier = True
            
        else:
            self.btn_barrier.label = 'Activate Light Barrier'
            self.barrier = False
    
    def LED_on_off(self):
        """Gets called on button click. Activates respectively deactivates LED corresponding to button label.
        """
        
        if self.btn_LED.label == 'Activate LED flash':
            self.btn_LED.label = 'Deactivate LED flash'
            GPIO.output(self.GPIO_Switch, GPIO.HIGH)

        else:
            self.btn_LED.label = 'Activate LED flash'
            GPIO.output(self.GPIO_Switch, GPIO.LOW)

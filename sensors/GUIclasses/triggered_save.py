import numpy as np
import scipy.signal as sg

from bokeh.plotting import figure
from bokeh.models import Panel, Button, ColumnDataSource, Slider, Div
from bokeh.models.widgets import RadioButtonGroup
from bokeh.events import ButtonClick
from bokeh.layouts import layout, gridplot, column, row, Spacer

from tornado import gen
import time


class TriggeredSaveTab:
    """Class to create a Bokeh tab that displays a life wingbeat stream of the latest audio chunk, the last four
    triggered signals and their PSD. The duration of the triggered signal can be indirectly specified with the
    `number_of_saved_chunks`, `chunk_size` and `sampling_rate` parameters in the :ref:`config.yaml` file.
    To trigger a signal the user can adjust a threshold and a gain.
    
    :param config: configuration including :ref:`config.yaml` dictionary and current Bokeh document
    :type config: object
    """
   
    def __init__(self, config):
        """config - configuration object
        """
        
        self.config = config
        self.chunk_size = config.chunk_size
        self.sampling_rate = config.sampling_rate
        self.timeframe = config.timeframe
        self.number_of_saved_chunks = config.number_of_saved_chunks
        self.decimation_factor = config.decimation_factor
        # derived parameters
        self.max_freq_khz = config.sampling_rate/self.decimation_factor/2 * 0.001
        self.timeslice = self.chunk_size/self.sampling_rate * 1000
        self.multiples = int(np.ceil(self.timeframe/self.timeslice))
        self.template = ("""<div class='content' style="background-color: {background};color: {colour};">
                            <div class='info'> {status_text} </div> </div>""")
        # call tab method to create tab 
        self._tab = self.make_tab()
        # create plot list to iterate through
        self.source_list = (self.trig_signal_source_1, self.trig_signal_source_2,
                            self.trig_signal_source_3, self.trig_signal_source_4,
                            self.spectrum_source_1, self.spectrum_source_2,
                            self.spectrum_source_3, self.spectrum_source_4)
        
        self.plot_counter = 0
        self.update_data = False
        self.trigger = False
        self.check_update = None
        
    @property
    def tab(self):
        """Calls method :meth:`make_tab`.
        """
        
        return self._tab
    
    def make_tab(self):
        """ Creates and arranges the elements of the corresponding Bokeh tab.
        
        :return: Bokeh panel for the visualization of four triggered signals 
        :rtype: Bokeh object
        """
        
        self.btn_wingbeats = Button(label='Start Stream', width=310)
        self.btn_wingbeats.on_event(ButtonClick, self.do_update)
        self.btn_reset_spectras = Button(label='Reset', width=25)
        self.btn_reset_spectras.on_event(ButtonClick, self.reset_spectras)
        self.text_status = Div(text=self.template.format(status_text='Wingbeat stream inactive',
                                                         background='#DC524C', colour='#FFFFFF'), render_as_text=False)
        PLOTARGS = dict(tools="", toolbar_location=None, outline_line_color='#595959')
        
        self.signal_source = ColumnDataSource(data=dict(t=[], y=[]))
        self.signal_plot = figure(plot_width=600, plot_height=200, title="Chunk signal",
                                  x_range=[0, self.timeslice], y_range=[-1, 1],
                                  x_axis_label='discrete time [ms]',
                                  y_axis_label='gain*normalized input [ ]', **PLOTARGS)
        self.signal_plot.background_fill_color = "#eaeaea"
        self.signal_plot.line(x="t", y="y", line_color="#024768", source=self.signal_source)
        
        self.trig_signal_source_1 = ColumnDataSource(data=dict(t=[], y=[]))
        self.trig_signal_plot_1 = figure(plot_width=400, plot_height=200, title="Triggered signal 1",
                                         x_range=[0, self.number_of_saved_chunks*self.timeslice], y_range=[-1, 1],
                                         x_axis_label='discrete time [ms]',
                                         y_axis_label='gain*normalized input [ ]', **PLOTARGS)
        self.trig_signal_plot_1.background_fill_color = "#eaeaea"
        self.trig_signal_plot_1.line(x="t", y="y", line_color="#024768", source=self.trig_signal_source_1)
        self.spectrum_source_1 = ColumnDataSource(data=dict(f=[], y=[]))
        self.spectrum_plot_1 = figure(plot_width=300, plot_height=200, title="PSD of triggered signal 1",
                                      y_range=[-130, -80], x_range=[0, self.max_freq_khz],
                                      x_axis_label='discrete frequency [kHz]', y_axis_label='[dB]', **PLOTARGS)
        self.spectrum_plot_1.background_fill_color = "#eaeaea"
        self.spectrum_plot_1.line(x="f", y="y", line_color="#024768", source=self.spectrum_source_1)
        
        self.trig_signal_source_2 = ColumnDataSource(data=dict(t=[], y=[]))
        self.trig_signal_plot_2 = figure(plot_width=400, plot_height=200, title="Triggered signal 2",
                                         x_range=[0, self.number_of_saved_chunks*self.timeslice], y_range=[-1, 1],
                                         x_axis_label='discrete time [ms]',
                                         y_axis_label='gain*normalized input [ ]', **PLOTARGS)
        self.trig_signal_plot_2.background_fill_color = "#eaeaea"
        self.trig_signal_plot_2.line(x="t", y="y", line_color="#024768", source=self.trig_signal_source_2)
        self.spectrum_source_2 = ColumnDataSource(data=dict(f=[], y=[]))
        self.spectrum_plot_2 = figure(plot_width=300, plot_height=200, title="PSD of triggered signal 2",
                                      y_range=[-130, -80], x_range=[0, self.max_freq_khz],
                                      x_axis_label='discrete frequency [kHz]', y_axis_label='[dB]', **PLOTARGS)
        self.spectrum_plot_2.background_fill_color = "#eaeaea"
        self.spectrum_plot_2.line(x="f", y="y", line_color="#024768", source=self.spectrum_source_2)
        
        self.trig_signal_source_3 = ColumnDataSource(data=dict(t=[], y=[]))
        self.trig_signal_plot_3 = figure(plot_width=400, plot_height=200, title="Triggered signal 3",
                                         x_range=[0, self.number_of_saved_chunks*self.timeslice], y_range=[-1, 1],
                                         x_axis_label='discrete time [ms]',
                                         y_axis_label='gain*normalized input [ ]', **PLOTARGS)
        self.trig_signal_plot_3.background_fill_color = "#eaeaea"
        self.trig_signal_plot_3.line(x="t", y="y", line_color="#024768", source=self.trig_signal_source_3)
        self.spectrum_source_3 = ColumnDataSource(data=dict(f=[], y=[]))
        self.spectrum_plot_3 = figure(plot_width=300, plot_height=200, title="PSD of triggered signal 3",
                                      y_range=[-130, -80], x_range=[0, self.max_freq_khz],
                                      x_axis_label='discrete frequency [kHz]', y_axis_label='[dB]', **PLOTARGS)
        self.spectrum_plot_3.background_fill_color = "#eaeaea"
        self.spectrum_plot_3.line(x="f", y="y", line_color="#024768", source=self.spectrum_source_3)
        
        self.trig_signal_source_4 = ColumnDataSource(data=dict(t=[], y=[]))
        self.trig_signal_plot_4 = figure(plot_width=400, plot_height=200, title="Triggered signal 4",
                                         x_range=[0, self.number_of_saved_chunks*self.timeslice], y_range=[-1, 1],
                                         x_axis_label='discrete time [ms]',
                                         y_axis_label='gain*normalized input [ ]', **PLOTARGS)
        self.trig_signal_plot_4.background_fill_color = "#eaeaea"
        self.trig_signal_plot_4.line(x="t", y="y", line_color="#024768", source=self.trig_signal_source_4)
        self.spectrum_source_4 = ColumnDataSource(data=dict(f=[], y=[]))
        self.spectrum_plot_4 = figure(plot_width=300, plot_height=200, title="PSD of triggered signal 4",
                                      y_range=[-130, -80], x_range=[0, self.max_freq_khz],
                                      x_axis_label='discrete frequency [kHz]', y_axis_label='[dB]', **PLOTARGS)
        self.spectrum_plot_4.background_fill_color = "#eaeaea"
        self.spectrum_plot_4.line(x="f", y="y", line_color="#024768", source=self.spectrum_source_4)
        # sliders for gain and threshold
        self.gain = Slider(start=1, end=300, value=200, step=10, title="Gain", default_size=200)
        self.threshold = Slider(start=0, end=1, value=0.5, step=0.025, title="Threshold", bar_color="#DC524C",
                                orientation='vertical', css_classes=["custom-slider2"], direction='rtl',
                                default_size=148)
        self.threshold.on_change('value', self.update_threshold)
        self.threshold_source = ColumnDataSource(data=dict(t=[0, self.timeslice + 5, self.timeslice + 5, 0],
                                                           y=[self.threshold.value, self.threshold.value,
                                                              -self.threshold.value, -self.threshold.value]))
        self.signal_plot.line(x="t", y="y", line_color="#DC524C", source=self.threshold_source)        
        # plot for all spectra of triggered signals
        self.all_spectra_source = ColumnDataSource(data=dict(f=[], y=[]))
        self.all_spectra_plot = figure(plot_width=500, plot_height=290, title="PSD of triggered signals",
                                       y_range=[-130, -80], x_range=[0, self.max_freq_khz],
                                       x_axis_label='discrete frequency [kHz]', y_axis_label='[dB]', **PLOTARGS)
        self.all_spectra_plot.background_fill_color = "#eaeaea"
        self.all_spectra_plot.line(x="f", y="y", line_color="#024768", source=self.all_spectra_source)
        
        self.rb_group = RadioButtonGroup(labels=['Max Signal', 'RMS Noise'], active=0, width=200)
        # define tab layout
        grid = gridplot([self.trig_signal_plot_1, self.spectrum_plot_1, self.trig_signal_plot_2,
                         self.spectrum_plot_2, self.trig_signal_plot_3, self.spectrum_plot_3,
                         self.trig_signal_plot_4, self.spectrum_plot_4], ncols=4, merge_tools=False)
        widgets_chunk = column([row(self.btn_wingbeats, self.text_status), row(Spacer(width=47), self.gain),
                                self.signal_plot])
        threshold_type = column(Div(text=self.template.format(status_text='Threshold Method',
                                                              background='#eaeaea', colour='black')),
                                self.rb_group, Spacer(height=13), self.threshold)
        all_spectra_block = row(self.all_spectra_plot, column(Spacer(height=22), self.btn_reset_spectras))
        # added because vertical orientation broken atm
        style = Div(text="""<style> """)
        _layout = layout(
            row(widgets_chunk, threshold_type, all_spectra_block, spacing=15),
            grid,
        )
        
        return Panel(child=_layout, title='WB - Triggered Save')

    @gen.coroutine
    def signal_update(self, data):
        """ Updates the chunk and the triggered signal figures. Targeted by next tick callback from
        :meth:`sensors.Utilities.pyaudiohandler.Pyaudiohandler.update_audio_data`. The PSD of the triggered signals
        get computed and plotted in the corresponding figures.

        :param data: resampled chunk signal, PSD of chunk signal, resampled ringbuffer, raw triggered signal and
        filename provided by :meth:`Pyaudiohandler.update_audio_data`
        :type data: 1D float array, 1D float array, 1D float array, 1D int16 array and string
        """
        
        if self.update_data:
            if data['values'] is None:
                return
            start = time.time()
            chunk_signal, _, _, triggered_signal, filename = data['values']
            # iterate through triggered signal plots
            if triggered_signal is not None and not self.check_update == filename:
                self.check_update = filename
                triggered_signal_resamp = sg.resample_poly(triggered_signal / 32768.0, 1, self.decimation_factor,
                                                           padtype='edge')
                PSD = 10*np.log10(sg.welch(triggered_signal_resamp, fs=self.sampling_rate/self.decimation_factor,
                                           window='hann', nperseg=512, noverlap=256+128)[1])
                # self.spectrum_plot_1.y_range.start = np.floor(min(PSD))
                # self.spectrum_plot_1.y_range.end = np.ceil(max(PSD))
                trig_source = self.source_list[self.plot_counter]
                t = np.linspace(0, self.number_of_saved_chunks*self.timeslice, len(triggered_signal_resamp))
                trig_source.data = dict(t=t, y=triggered_signal_resamp*self.gain.value)
                # print(time.time()-start)
                spectrum_source = self.source_list[self.plot_counter + 4]
                f = np.linspace(0, self.max_freq_khz, len(PSD))
                spectrum_source.data = dict(f=f, y=PSD)
                color = ["#024768", "red", "green", "magenta"]
                self.all_spectra_plot.line(x="f", y="y", line_color=color[self.plot_counter],
                                           source=dict(f=f, y=PSD))
                self.plot_counter += 1
                if self.plot_counter == 4:
                    self.plot_counter = 0
            # the if-else below are small optimization: avoid computing and sending
            # all the x-values, if the length has not changed
            if len(chunk_signal) == len(self.signal_source.data['y']):
                self.signal_source.data['y'] = chunk_signal*self.gain.value
            else:
                t = np.linspace(0, self.timeslice, len(chunk_signal))
                self.signal_source.data = dict(t=t, y=chunk_signal*self.gain.value)

    def do_update(self):
        """ Gets called on button click. Starts respectively ends the preview of chunk signal and
        changes the text status corresponding to the button label. Triggered signals only get saved
        when preview is active.
        """
        
        if self.btn_wingbeats.label == 'Start Stream':
            self.btn_wingbeats.label = 'Stop Stream'
            self.text_status.text = self.template.format(status_text='Wingbeat stream active', colour='#FFFFFF',
                                                         background='#3EA639')
            self.update_data = True
            self.trigger = True
        else:
            self.btn_wingbeats.label = 'Start Stream'
            self.text_status.text = self.template.format(status_text='Wingbeat stream inactive', colour='#FFFFFF',
                                                         background='#DC524C')
            self.update_data = False
            self.trigger = False

    def update_threshold(self, attr, old, new):
        """Gets called on change of value of threshold slider and updates the threshold line in the chunk signal figure.
        """
        
        self.threshold_source.data['y'] = [self.threshold.value, self.threshold.value,
                                           -self.threshold.value, -self.threshold.value]

    def reset_spectras(self):
        """Gets called on button click and resets the figure of accumulated PSD lines of triggered signals.
        """
        
        lines = []
        for line in self.all_spectra_plot.renderers:
            lines.append(line)
        for line in lines:
            self.all_spectra_plot.renderers.remove(line)

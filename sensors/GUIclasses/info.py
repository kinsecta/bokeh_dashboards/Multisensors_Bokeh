from bokeh.models import Panel
from bokeh.layouts import column
from bokeh.models import Div

from os.path import dirname, join
import os
import sys
import subprocess
import socket
import pyudev


class InfoTab:
    """Class to create a Bokeh tab that displays information about the system and the peripherical devices.
    
    :param init_dev: picamera and audio device object initialized in :mod:`sensors.app_hooks` with
    :class:`sensors.Utilities.initdevices.InitDevices`
    :type init_dev: object
    """
    
    def __init__(self, init_dev):
        
        self.camera = init_dev.camera
        self.audio_devices = init_dev.audio_devices
        
        self.system_info = Div(text=self.get_system_info(),
                               render_as_text=False, width=1000)

        self.camera_info = Div(text=self.get_camera_info(),
                               render_as_text=False, width=1000)
        
        self.audio_info = Div(text=self.get_audio_info(),
                              render_as_text=False, width=1000)

        # self.storage_info = Div(text=self.get_storage_info(),
        # render_as_text=False, width=1000)
        
        # Information tab header loaded from html template
        filename = join(dirname(__file__), "info_template.html")
        desc = Div(text=open(filename).read(),
                   render_as_text=False, width=800)
        layout = column(desc, self.system_info, self.camera_info, self.audio_info)

        # Make a tab with the layout
        self._tab = Panel(child=layout, title='Information')
        
    @property
    def tab(self):
        """Creates and arranges the elements of the corresponding Bokeh tab.
     
        :return: Bokeh panel for visualization of general information on system and devices 
        :rtype: Bokeh object
        """
        
        return self._tab

    @staticmethod
    def get_system_info():
        """Creates general system information.
        
        :return: html code for corresponding information
        :rtype: string
        """
        
        info = '<h2>System Information</h2>\n'
        info += '<table style="width:100%; margin: 0 20px 0 0">'
        info += '<tr><td>Operating system: </td><td>'f'{sys.platform}''</td></tr>'
        info += '<tr><td>Python version:</td><td>' + sys.version.replace('\n', '') + '</td></tr>'
        info += '<tr><td>Hostname:</td><td>'f'{socket.gethostname()}''</td></tr>'
        try:
            login = f'{os.getlogin()}'
        except Exception as e:
            login = ''
        info += '<tr><td>Login name:</td><td>' + login + '</td></tr>'
        info += '</table>'
        
        return info
    
    def get_camera_info(self):
        """Creates PiCamera information.

        :return: html code for corresponding information
        :rtype: string
        """
        
        info = '<h2>Camera Information</h2>\n'
        if self.camera is None:
            info = '<h2>No PiCamera found!<h2>'
        else:
            resolution = self.camera.MAX_RESOLUTION
            info += '<table style="width:100%; margin: 0 20px 0 0">'
            info += '<tr><td>PiCamera type:</td><td>'f'{self.camera.revision}''</td></tr>'
            info += '<tr><td>Maximum resolution:</td><td>'f'{resolution.width} x {resolution.height}''</td></tr>'
            info += '</table>'
        
        return info
    
    def get_audio_info(self):
        """Creates audio system information.

        :return: html code for corresponding information
        :rtype: string
        """
        
        info = '<h2>Audio Device Information</h2>\n'
        speaker, mic = self.audio_devices
        info += '<table style="width:100%; margin: 0 20px 0 0">'
        if mic is not None:
            info += '<tr><td>Microphone input ('f'{mic.channels}'' channels):</td><td>'f'{mic.id}''</td></tr>'
        else:
            info += '<tr><td>No USB microphone found!</td></tr>'
        if speaker is not None:
            info += '<tr><td>Speaker output ('f'{speaker.channels}'' channels):</td><td>'f'{speaker.id}''</td></tr>'
        else:
            info += '<tr><td>No USB speaker found!</td></tr>'
        info += '</table>'
        
        return info

from bokeh.models import AutocompleteInput
from bokeh.models import Button, Select, Slider, NumericInput, TextInput
from bokeh.events import ButtonClick
from bokeh.layouts import row, column


class CameraSettings:
    """Class to create a Bokeh widget to change Picamera settings, including analog gain, digital gain
    and automatic white balance (awb) for red and blue channels.
       
    :param config: configuration including presettings for Picamera and current Bokeh document
    :type config: object
    """
    
    def __init__(self, config):
        
        self.config = config
        self.curdoc = config.curdoc
               
        self._widget = self.make_widget()

    @property
    def widget(self):
        """Calls method :meth:`make_widget`.
        """
        
        return self._widget

    def make_widget(self):
        """ Creates the Picamera settings widget, including sliders and numeric inputs to change camera parameters.
        
        :return: Bokeh widget for Picamera settings
        :rtype: Bokeh object
        """
        
        MARGIN = (10, 0, 0, 5)
        WIDTH_INPUT = 50
        WIDTH_SLIDER = 133
        self.btn_show_settings = Button(label='Show Camera Settings', width=310)
        self.btn_show_settings.on_event(ButtonClick, self.show_settings)

        # define sliders, numeric inputs and select widget for RGB values of LED stripe - jslink corresponding inputs
        self.shutter_speed_Input = NumericInput(width=WIDTH_INPUT, value=self.config.exposure_time, low=1000,
                                                high=int(1/self.config.fps_preview*1000000),
                                                mode='int', css_classes=["custom-input"])
        self.shutter_speed = Slider(start=1000, end=int(1/self.config.fps_preview*1000000),
                                    value=self.shutter_speed_Input.value, step=1000,
                                    bar_color='#eaeaea', css_classes=["custom-slider1"], default_size=WIDTH_SLIDER,
                                    show_value=False, margin=MARGIN)
        self.analog_gain_Input = NumericInput(width=WIDTH_INPUT, value=self.config.analog_gain, low=0,
                                              high=8, mode='float')
        self.analog_gain = Slider(start=0, end=8, value=self.analog_gain_Input.value, step=0.1,
                                  bar_color='#eaeaea', css_classes=["custom-slider1"], default_size=WIDTH_SLIDER,
                                  show_value=False, margin=MARGIN)
        self.digital_gain_Input = NumericInput(width=WIDTH_INPUT, value=self.config.digital_gain,
                                               low=0, high=8, mode='float')
        self.digital_gain = Slider(start=0, end=8, value=self.digital_gain_Input.value, step=0.1,
                                   bar_color='#eaeaea', css_classes=["custom-slider1"], default_size=WIDTH_SLIDER,
                                   show_value=False, margin=MARGIN)
        self.white_balance_Input_1 = NumericInput(width=WIDTH_INPUT, value=eval(self.config.white_balance)[0],
                                                  low=0, high=8, mode='float')
        self.white_balance_1 = Slider(start=0, end=8, value=self.white_balance_Input_1.value, step=0.1,
                                      default_size=WIDTH_SLIDER, bar_color='#eaeaea', css_classes=["custom-slider1"],
                                      show_value=False, margin=MARGIN)
        self.white_balance_Input_2 = NumericInput(width=WIDTH_INPUT, value=eval(self.config.white_balance)[1],
                                                  low=0, high=8, mode='float')
        self.white_balance_2 = Slider(start=0, end=8, value=self.white_balance_Input_2.value, step=0.1,
                                      default_size=WIDTH_SLIDER, bar_color='#eaeaea', css_classes=["custom-slider1"],
                                      show_value=False, margin=MARGIN)
        
        WIDTH_TEXTS = 107
        shutter_speed_Text = TextInput(width=WIDTH_TEXTS, value='Exposure Time', css_classes=["custom-input"])
        shutter_speed_Text.disabled = True
        analog_gain_Text = TextInput(width=WIDTH_TEXTS, value='Analog Gain', css_classes=["custom-input"])
        analog_gain_Text.disabled = True
        digital_gain_Text = TextInput(width=WIDTH_TEXTS, value='Digital Gain', css_classes=["custom-input"])
        digital_gain_Text.disabled = True
        white_balance_Text_1 = TextInput(width=WIDTH_TEXTS, value='White Balance R', css_classes=["custom-input"])
        white_balance_Text_1.disabled = True
        white_balance_Text_2 = TextInput(width=WIDTH_TEXTS, value='White Balance B', css_classes=["custom-input"])
        white_balance_Text_2.disabled = True
        
        self.settings_pre_select = Select(title='Predefined Settings:', value='Default - LED Flash',
                                          options=['Default - LED Flash', 'Default - No LED Flash', 'Custom'],
                                          width=200)
        self.settings_pre_select.on_change('value', self.set_select_values)

        self.shutter_speed_Input.js_link('value', self.shutter_speed, 'value')
        self.shutter_speed.js_link('value', self.shutter_speed_Input, 'value')
        
        self.analog_gain_Input.js_link('value', self.analog_gain, 'value')
        self.analog_gain.js_link('value', self.analog_gain_Input, 'value')
        
        self.digital_gain_Input.js_link('value', self.digital_gain, 'value')
        self.digital_gain.js_link('value', self.digital_gain_Input, 'value')
        
        self.white_balance_Input_1.js_link('value', self.white_balance_1, 'value')
        self.white_balance_1.js_link('value', self.white_balance_Input_1, 'value')
        
        self.white_balance_Input_2.js_link('value', self.white_balance_2, 'value')
        self.white_balance_2.js_link('value', self.white_balance_Input_2, 'value')
       
        # row(shutter_speed_Text, self.shutter_speed_Input, self.shutter_speed),
        all_settings = column(row(analog_gain_Text, self.analog_gain_Input, self.analog_gain),
                              row(digital_gain_Text, self.digital_gain_Input, self.digital_gain),
                              row(white_balance_Text_1, self.white_balance_Input_1, self.white_balance_1),
                              row(white_balance_Text_2, self.white_balance_Input_2, self.white_balance_2),
                              spacing=25)
        
        # self.settings = column(all_settings, self.settings_pre_select, spacing=25)
        self.settings = all_settings
        self.settings.visible = False
        
        return column(self.btn_show_settings, self.settings)

    def show_settings(self):
        """Gets called on button click to show/hide camera setting sliders and numeric inputs.
        """
        
        if self.btn_show_settings.label == 'Show Camera Settings':
            self.btn_show_settings.label = 'Hide Camera Settings'
            self.settings.visible = True
            
        else:
            self.btn_show_settings.label = 'Show Camera Settings'
            self.settings.visible = False
    
    def set_select_values(self, attr, new, old):
        """Gets called on change of select widget's value and loads predefined values for the camera parameters.
        """
        
        if old == 'Default - LED Flash':
            self.shutter_speed.value, self.analog_gain.value = (self.config.exposure_time, self.config.analog_gain())
            self.digital_gain.value, self.white_balance_1.value = (self.config.digital_gain,
                                                                   eval(self.config.white_balance)[0])
            self.white_balance_2.value = eval(self.config.white_balance)[1]
        elif old == 'Default - No LED Flash':
            self.shutter_speed.value, self.analog_gain.value = int(1/self.config.fps_preview*1000000), 2
            self.digital_gain.value, self.white_balance_1.value = 2, 2.25
            self.white_balance_2.value = 2.25
        else:
            print('Load Values from file (not implemented yet)!')
